package vastro.com.testapp.interfaces;


import vastro.com.testapp.model.Country;

public interface RecyclerItemClickListener {
    void onItemClick(Country country);
}