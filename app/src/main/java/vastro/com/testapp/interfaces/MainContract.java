package vastro.com.testapp.interfaces;


import java.util.ArrayList;

import vastro.com.testapp.model.Country;

public interface MainContract {

    /**
     * Call when user interact with the view and other when view OnDestroy()
     * */
    interface presenter{

        void onDestroy();

        void onRefreshButtonClick();

        void requestDataFromServer();

    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetCountryInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setTitle(String title);

        void setDataToRecyclerView(ArrayList<Country> countryArrayList);

        void onResponseFailure(Throwable throwable);

    }

    /**
     * Intractors are classes built for fetching data from your database, web services, or any other data source.
     **/
    interface GetCountryIntractor {

        interface OnFinishedListener {
            void onFinished(ArrayList<Country> countryArrayList, String title);
            void onFailure(Throwable t);
        }

        void getCountryArrayList(OnFinishedListener onFinishedListener);
    }
}
