package vastro.com.testapp.network;


import retrofit2.Call;
import retrofit2.http.GET;
import vastro.com.testapp.model.CountryList;

public interface GetCountryDataService {

    @GET("facts.json")
    Call<CountryList> getCountryData();

}