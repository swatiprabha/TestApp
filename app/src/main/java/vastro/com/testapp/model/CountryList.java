package vastro.com.testapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CountryList implements Parcelable{

    @Expose
    @SerializedName("rows")
    private ArrayList<Country> countryList;
    @SerializedName("title")
    @Expose
    private String title;

    protected CountryList(Parcel in) {
        countryList = in.createTypedArrayList(Country.CREATOR);
        title = in.readString();
    }

    public static final Creator<CountryList> CREATOR = new Creator<CountryList>() {
        @Override
        public CountryList createFromParcel(Parcel in) {
            return new CountryList(in);
        }

        @Override
        public CountryList[] newArray(int size) {
            return new CountryList[size];
        }
    };

    public ArrayList<Country> getCountryArrayList() {
        return countryList;
    }

    public void setNoticeArrayList(ArrayList<Country> countryArrayList) {
        this.countryList = countryArrayList;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(countryList);
        parcel.writeString(title);
    }
}