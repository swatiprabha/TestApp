package vastro.com.testapp.util;

import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import vastro.com.testapp.R;


public class ConnectionDetector {

    private Context _context;

    public ConnectionDetector(Context context) {
        this._context = context;
    }


    public boolean isConnectingToInternet(Context context) {
        this._context = context;
        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }


    public void showInternetErrorDialog() {
        try {
            final Dialog dialog = new Dialog(_context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_nointernet);

            TextView tv_ok = (TextView) dialog.findViewById(R.id.tv_ok);
            TextView tv_retry = (TextView) dialog.findViewById(R.id.tv_retry);
            TextView title = (TextView) dialog.findViewById(R.id.title_dialog);
            TextView message = (TextView) dialog.findViewById(R.id.message_dialog);
            View vVerLine = (View) dialog.findViewById(R.id.vVerLine);

            title.setText("No Internet !!");
            message.setText("Please check your internet connection.");


            tv_ok.setOnClickListener(v -> dialog.dismiss());
            dialog.show();


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}