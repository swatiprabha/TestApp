package vastro.com.testapp.util;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import vastro.com.testapp.model.Country;


public class CacheData {

    private Context _context;

    public CacheData(Context context) {
        this._context = context;
    }


    public static final String PREFS_NAME = "TEST_APP";
    public static final String COUNTRIES = "countries";

    public void storeCountries(List countries) {
        // used for store arrayList in json format
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = _context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson sExposeGson = builder.create();
        String json = sExposeGson.toJson(countries);
        editor.putString(COUNTRIES, json);
        editor.commit();
    }


    public ArrayList loadCountries() {
        // used for retrieving arraylist from json formatted string
        SharedPreferences settings;
        List favorites;
        settings = _context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        if (settings.contains(COUNTRIES)) {
            String jsonFavorites = settings.getString(COUNTRIES, null);

            GsonBuilder builder = new GsonBuilder();
            builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
            builder.excludeFieldsWithoutExposeAnnotation();
            Gson sExposeGson = builder.create();
            Country[] favoriteItems = sExposeGson.fromJson(jsonFavorites, Country[].class);
            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList(favorites);
        } else
            return null;
        return (ArrayList) favorites;
    }

}