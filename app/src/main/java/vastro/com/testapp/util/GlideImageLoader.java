package vastro.com.testapp.util;

import android.content.Context;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;


import vastro.com.testapp.R;


public class GlideImageLoader {

    public static void loadFromDrawable(Context context, int resId, ImageView iv) {
        Glide.with(context)
                .load(resId)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(iv);

    }

    public static void loadFromURl(Context context, String url, ImageView iv) {
        Glide.with(context)
                .load(url)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .into(iv);
    }

}
