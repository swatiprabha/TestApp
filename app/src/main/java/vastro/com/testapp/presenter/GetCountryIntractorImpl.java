package vastro.com.testapp.presenter;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vastro.com.testapp.interfaces.MainContract;
import vastro.com.testapp.model.Country;
import vastro.com.testapp.model.CountryList;
import vastro.com.testapp.network.GetCountryDataService;
import vastro.com.testapp.network.RetrofitInstance;

public class GetCountryIntractorImpl implements MainContract.GetCountryIntractor {

    @Override
    public void getCountryArrayList(final OnFinishedListener onFinishedListener) {

        /** Create handle for the RetrofitInstance interface*/
        GetCountryDataService service = RetrofitInstance.getRetrofitInstance().create(GetCountryDataService.class);

        /** Call the method with parameter in the interface to get the country data*/
        Call<CountryList> call = service.getCountryData();

        /**Log the URL called*/
        Log.wtf("URL Called", call.request().url() + "");

        call.enqueue(new Callback<CountryList>() {
            @Override
            public void onResponse(Call<CountryList> call, Response<CountryList> response) {
                if (response != null) {
                    ArrayList<Country> dataList=new ArrayList<>();
                    for(Country country : response.body().getCountryArrayList()) {
                        if(country.getTitle()==null && country.getDescription()==null && country.getImageHref()==null){
                        }
                        else{
                            dataList.add(new Country(country.getTitle(),country.getDescription(),country.getImageHref()));
                        }
                    }
                    onFinishedListener.onFinished(dataList,response.body().getTitle());
                }
            }
            @Override
            public void onFailure(Call<CountryList> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }
}
