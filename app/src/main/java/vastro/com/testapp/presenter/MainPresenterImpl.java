package vastro.com.testapp.presenter;


import java.util.ArrayList;

import retrofit2.Response;
import vastro.com.testapp.interfaces.MainContract;
import vastro.com.testapp.model.Country;
import vastro.com.testapp.model.CountryList;
import vastro.com.testapp.util.ConnectionDetector;


public class MainPresenterImpl implements MainContract.presenter, MainContract.GetCountryIntractor.OnFinishedListener {

    private MainContract.MainView mainView;
    private MainContract.GetCountryIntractor getCountryIntractor;

    public MainPresenterImpl(MainContract.MainView mainView, MainContract.GetCountryIntractor getCountryIntractor) {
        this.mainView = mainView;
        this.getCountryIntractor = getCountryIntractor;
    }

    @Override
    public void onDestroy() {
        mainView = null;
    }

    @Override
    public void onRefreshButtonClick() {
        FetchingData();
    }

    private void FetchingData() {
        if(mainView != null){
            mainView.showProgress();
        }
        getCountryIntractor.getCountryArrayList(this);
    }

    @Override
    public void requestDataFromServer() {
        FetchingData();
    }


    @Override
    public void onFinished(ArrayList<Country> countryArrayList, String title) {
        if(mainView != null){
            mainView.setDataToRecyclerView(countryArrayList);
            mainView.hideProgress();
            mainView.setTitle(title);
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(mainView != null){
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}
