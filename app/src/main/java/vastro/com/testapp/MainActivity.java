package vastro.com.testapp;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;
import vastro.com.testapp.adapter.CountryAdapter;
import vastro.com.testapp.interfaces.MainContract;
import vastro.com.testapp.interfaces.RecyclerItemClickListener;
import vastro.com.testapp.model.Country;
import vastro.com.testapp.model.CountryList;
import vastro.com.testapp.presenter.GetCountryIntractorImpl;
import vastro.com.testapp.presenter.MainPresenterImpl;
import vastro.com.testapp.util.CacheData;
import vastro.com.testapp.util.ConnectionDetector;

public class MainActivity extends AppCompatActivity implements MainContract.MainView{

    @BindView(R.id.rv_country)RecyclerView recyclerView;
    @BindView(R.id.toolbar)Toolbar toolbar;
    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    private MainContract.presenter presenter;
    private ProgressBar progressBar;
    private ConnectionDetector connectionDetector;
    private List<Country> dataList=new ArrayList<>();

    public CacheData cacheData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initializeToolbarAndRecyclerView();
        initProgressBar();

        presenter = new MainPresenterImpl(this, new GetCountryIntractorImpl());
        if(connectionDetector.isConnectingToInternet(MainActivity.this)) {
            presenter.requestDataFromServer();
        }else{
           // If Internet is not there , will fetch from local Db
            SettingDataFromCache();
        }
    }

    /**
     * Initializing Toolbar and RecyclerView
     */
    private void initializeToolbarAndRecyclerView() {
        cacheData=new CacheData(MainActivity.this);
        connectionDetector=new ConnectionDetector(MainActivity.this);
        setSupportActionBar(toolbar);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //Swipe To Refresh
        swipeRefreshLayout.setOnRefreshListener(() -> {
            RefreshData();
        });
    }

    /**
     * Initializing progressbar programmatically
     * */
    private void initProgressBar() {
      //  CountryDatabase.getInstance(MainActivity.this);
        progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);

        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setGravity(Gravity.CENTER);
        relativeLayout.addView(progressBar);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        progressBar.setVisibility(View.INVISIBLE);

        this.addContentView(relativeLayout, params);
    }

    /**
     * RecyclerItem click event listener
     * */
    private RecyclerItemClickListener recyclerItemClickListener = country -> {
    };

    @Override
    public void showProgress() {
        if(!swipeRefreshLayout.isRefreshing()) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void setTitle(String title) {
        toolbar.setTitle(title);
    }


    @Override
    public void setDataToRecyclerView(ArrayList<Country> countryArrayList) {
        if(swipeRefreshLayout.isRefreshing()){
            swipeRefreshLayout.setRefreshing(false);
        }
        cacheData.storeCountries(countryArrayList);
        SettingAdapter(countryArrayList);
    }


    private void SettingAdapter(ArrayList<Country> dataList) {
        CountryAdapter adapter = new CountryAdapter(MainActivity.this, dataList, recyclerItemClickListener);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Toast.makeText(MainActivity.this, "Something went wrong...Error message: " + throwable.getMessage(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            RefreshData();
        }
        return true;
    }

    private void RefreshData() {
        if(connectionDetector.isConnectingToInternet(MainActivity.this)) {
            presenter.onRefreshButtonClick();
        }else{
            if(swipeRefreshLayout.isRefreshing()){
                swipeRefreshLayout.setRefreshing(false);
            }
            SettingDataFromCache();
        }
    }

    private void SettingDataFromCache() {
        try {
            new CountryAsyncTask().execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }


    private class CountryAsyncTask extends AsyncTask<Void, Void, ArrayList<Country>> {
        @Override
        protected ArrayList<Country> doInBackground(Void... params) {
         return cacheData.loadCountries();
        }
        @Override
        protected void onPostExecute(ArrayList<Country> list) {
          dataList=list;
          if(dataList!=null) {
              if (dataList.size() > 0) {
                  SettingAdapter((ArrayList) dataList);
              }
          }else{
                connectionDetector.showInternetErrorDialog();
            }
        }
    }

}
