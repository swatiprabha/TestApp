package vastro.com.testapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import vastro.com.testapp.R;
import vastro.com.testapp.interfaces.RecyclerItemClickListener;
import vastro.com.testapp.model.Country;
import vastro.com.testapp.util.GlideImageLoader;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryViewHolder>{

    private ArrayList<Country> dataList;
    private RecyclerItemClickListener recyclerItemClickListener;
    private Context context;

    @NonNull
    @Override
    public CountryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.country_row, parent, false);
        return new CountryViewHolder(view);
    }

    public CountryAdapter(Context context, ArrayList<Country> dataList, RecyclerItemClickListener recyclerItemClickListener) {
        this.dataList = dataList;
        this.recyclerItemClickListener = recyclerItemClickListener;
        this.context=context;
    }

    @Override
    public void onBindViewHolder(CountryViewHolder holder, final int position) {
        Country country =dataList.get(position);

        SetData(holder, country);
        holder.itemView.setOnClickListener(v -> recyclerItemClickListener.onItemClick(dataList.get(position)));
    }

    private void SetData(CountryViewHolder holder, Country country) {

        //Setting Title
        if(country.getTitle()==null || country.getTitle().isEmpty()){
            holder.txt_title.setVisibility(View.GONE);
        }
        else{
            holder.txt_title.setVisibility(VISIBLE);
            holder.txt_title.setText(country.getTitle());
        }

        //Setting Description
        if(country.getDescription()==null || country.getDescription().isEmpty()){
            holder.txt_desc.setVisibility(View.GONE);
        }
        else{
            holder.txt_desc.setVisibility(VISIBLE);
            holder.txt_desc.setText(country.getDescription());
        }

        //Setting Image
        if(country.getImageHref()==null || country.getImageHref().isEmpty()) {
            holder.img_pic.setVisibility(GONE);
        }
        else{
            holder.img_pic.setVisibility(VISIBLE);
            GlideImageLoader.loadFromURl(context,country.getImageHref(),holder.img_pic);
        }
    }
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class CountryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_title)
        TextView txt_title;
        @BindView(R.id.txt_desc) TextView txt_desc;
        @BindView(R.id.cv_main)
        CardView cv_main;
        @BindView(R.id.img_pic)
        ImageView img_pic;
        CountryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
